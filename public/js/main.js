(function() {

  var APP = angular.module("myAPP",['angular-loading-bar','ngRoute', 'ngAnimate','smoothScroll','ngTouch','ngCookies']);

    APP.controller("MainController",function($scope,$location,$cookies) {

        this.langage = ($cookies.get('saved_language')) ? $cookies.get('saved_language') : "FR";
        if($cookies.get('saved_language') == null) {
            $cookies.put('saved_language','EN');
        }
        this.langage_store = {
            "FR" :  {
                accueil : "Accueil",
                contact : "Nous contacter",
                services : "Nos services",
                clients : "Nos clients",
                equipe : "Notre équipe"
            },
            "EN" :  {
                accueil : "Home",
                contact : "Contact us",
                services : "Our services",
                clients : "Our clients",
                equipe : "Our team"
            },
            "CH" :  {
                accueil : "歡迎",
                contact : "聯繫我們",
                services : "我們的服務",
                clients : "我們的客戶",
                equipe : "我們的團隊"
            }
        }
        $scope.langue = this.langage_store[this.langage];

        this.indexStorage = {
            "/home" : 1,
            "/" : 1,
            "/contact" : 2,
            "/nos-services" : 3,
            "/clients" : 4,
            "/equipe" : 5
        }
        this.path = $location.path();
        $scope.animationDirection = false;
        $scope.pageIndex = this.indexStorage[this.path];

        this.swap = function(page,index) {
            console.log("swap");
            if(this.path != "/"+page) {
                $scope.animationDirection = (index >= $scope.pageIndex) ? true : false;
                $location.path(page);
                this.path = $location.path();
                $scope.pageIndex = this.indexStorage[this.path];
            }
        }

        this.changeLanguage = function(focus) {
            if(this.langage != focus) {
                this.langage = focus;
                $scope.langue = this.langage_store[this.langage];
                $cookies.put('saved_language',focus);
            }
        }

    });

    APP.controller("ContactController",function($scope,$http) {
        $scope.formData = {};
        this.submit = function() {
            console.log("form submit");
        }
    });

    APP.config(['$routeProvider', function ($routeProvider) {
      $routeProvider
      .when('/', {
          templateUrl: 'partials/home'
      })
      .when('/contact', {
          templateUrl: "partials/contact"
      })
      .when('/clients', {
          templateUrl: "partials/clients"
      })
      .when('/nos-services', {
          templateUrl: "partials/nos-services"
      })
      .when('/equipe', {
          templateUrl: "partials/equipe"
      })
      .otherwise({
          redirectTo: '/'
      });
    }]);
})();
